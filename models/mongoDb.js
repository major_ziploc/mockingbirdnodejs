// Mongo Database connector.
var mongoose = require('mongoose');
// Connect to DB
mongoose.connect('mongodb://test:best@ds153494.mlab.com:53494/mockingbirdnodejs');
// Create db schema
var imgSchema = new mongoose.Schema({
  img: { data: Buffer, contentType: String }
});

const userSchema = new mongoose.Schema({
    userinfo: {
      password: String,
      username: String,
    }
});

// Create db model using collection name and schema
var ImageModel = mongoose.model('images', imgSchema);

const UserModel = mongoose.model('users', userSchema);

module.exports = {
  ImageModel: ImageModel,
  UserModel: UserModel
}
