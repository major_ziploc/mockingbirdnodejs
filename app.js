//Express routing system
var express = require('express');

// Session system for managing user sessions
const session = require('client-sessions');

// Set controllers
const indexController = require('./controllers/indexController');

// Get app controller structure.
const app = express();

//EJS View Engine
app.set('view engine', 'ejs');

app.use('/assets', express.static('assets'));

app.use(session({
  cookieName: 'session',
  secret: 'random_string_goes_here',
  duration: 30*60*1000,
  activeDuration: 5*60*1000,
}));

// Fire controllers
indexController(app);

// listen to port
app.listen(3000);

console.log('Listening to port 3000');
