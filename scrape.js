
// Where the fun starts

// This function is the one that will take your URL and scrape the page for all of the information on that bird.
// amount of paragraphs gotten can be adjusted by not using all elements in the URLS variable.
// SIDE NOTE: yes I'm aware that "url" is a horrible variable name but i was trying to NOT botch the tutorial i was basing this on
function Scraper(urlString, callback){
/*
-Standard variable setup that will be needed for the function
-"request" and "cheerio" are libraries/dependencies (whatever this language used)
-"urls" hold the raw text from the web page
-"fixedStrings" holds the fixedStrings from the webpage
*/
var request = require('request'),
    cheerio = require('cheerio'),
    urls = [],
    fixedStrings = [];

// test URL = https://birdsna.org/Species-Account/bna/species/blujay/introduction I know this once works. Keeping it here just in case

/*
Request the info from the webpage with the URL.
Use the functions from cheerio in the form of JQuery i belive it is to pull text
*/
request(urlString, function(err, resp, body){
    // "if" conditional checks the status code to see if its 200 which in html means its OK
    if(!err && resp.statusCode == 200){

        // "cheerio" then loads the text body
        var $ = cheerio.load(body);

        // For all the parenthesis blocks IN THE SCOPE of id = overview which every page on this website has.
        // Decreases range of search so therefore increases performance.
        $('p', '#overview').each(function(){

            //rip the paragraph block in html("url")
            var url = $(this)
            // Rip just the text
            var urlText = url.text()
            // push text into "urls array"
            urls.push(urlText);

        });
        // Establishing variables. Names ae what the do. Think there may be hidden error with indexOf vars being set to -1
        // But i've never experienced any and it gives me the correct output with good and bad inputs. And if it has or doesn't
        // Have parenthesis in the block of text the codes working with
        var str = '',
        indexOfOpenBracket = -1,
        indexOfClosedBracket = -1,
        stringHolder = '';


        var brackets;
        for(var i = 0; i < urls.length; i++){
            // Initialize buckets in for loop to [0] to reset index of parenthesis for that block of text
            brackets = [0]
            // Pulls the string to work with. Str doesn't stand for Strength. This is D&D or and RPG
            str = urls[i]

            // Will iterate through the text to get the index of the parenthesis. Again i know bad naming convention with "brackets"
            while (str.indexOf('(', brackets[brackets.length-1] + 1) != -1
            ){
                // Get open Parenthesis index
                indexOfOpenBracket = str.indexOf('(', brackets[brackets.length-1])
                // Get closed Parenthesis index
                indexOfClosedBracket = str.indexOf(')', brackets[brackets.length-1] + 1)

               // Ok little tricky here but the logic works. This is here for the while loop below this.
               // It gets the index of the NEXT open parenthesis. LOOK AT WHILE COMMENTS AS TO WHY
                var temp = str.indexOf('(', indexOfOpenBracket + 1)
                // Throw open parenthesis into the array. Yay! Can push up here because of how i search for parenthesis.
                // It enforces that the open parenthesis will ALWAYS be right
                brackets.push(indexOfOpenBracket)

                /*
                Ok this is why wee needed temp. This while loops checks for NESTED parenthesis in the parenthesis.
                Some of the files actually have this problem and is how i found out about it.
                Wee check to see if the index of the NEXT open parenthesis is greater than that of the closed because if it is
                then that means we have escaped the nested parenthesis. Wee Wooh
                */
                while (indexOfClosedBracket > temp) {
                    // if there is not parenthesis after the last one we checked break this loop
                    if (temp == -1) {
                        break
                    } else {
                    // Theres parenthesis after this one so grab the next set of parenthesis and check those
                    indexOfClosedBracket = str.indexOf(')', indexOfClosedBracket + 1)
                    temp = str.indexOf('(', temp + 1)
                    }

                }
                // finally having the correct close parenthsis push that into the stack
                brackets.push(indexOfClosedBracket)

            }
            // grab the index of the last character in the string
            brackets.push(str.length - 1);
            // send it to string fixer to make the strings look pretty
            stringHolder = StringFixer(str, brackets);
            // puts the fixed strings into its own array as the function above gets called for every paragraph
            fixedStrings.push(stringHolder);




        }
        // forces the function to get an input
        Returner(fixedStrings, callback);


    }
});

}
// Does what it says. Fixes the string blocks it gets. Wee give it the string block and
// the list of what the parenthesis are located and it cuts out the parenthesis and everything between them
function StringFixer(str, arrayBrackets){
    // precaution just to make sure wee don't get anything funky. I've never seen this error actually fire though
    if (arrayBrackets.length % 2 != 0) {
        console.log("Error Uneven parenthesis")
        return ""
    }
    // Storage
    var stringHold = ""
    // Where we start going through the string and fixing it
    for(var i = 0; i < arrayBrackets.length; i = i + 2) {

        if (i == 0) {
            // Unique case since we have to include the first character since its the start of the string
            // substring used to chop the string up into pieces that stringHold gets and they are 'glued' together
            stringHold = stringHold + str.substring(arrayBrackets[i], arrayBrackets[i+1])


        } else {
            // Same as one before except the plus 1 in the first parameter is to make sure we don't capture the parenthesis
            // sustring is (inclusive, non inclusive) so we don't have to worry about that with the second parameter
            // SIDE NOTE i belive thats right but could also be wrong and could just be capturing an extra space that gets deleted here
            stringHold = stringHold + str.substring(arrayBrackets[i] + 2, arrayBrackets[i+1])

        }
    }
    // give back the beautimus string
    return stringHold
}

//This is the fucntion that will search through all the bird names and get the URL extension for that bird
function GetSpeciesURLS(stringBirdName, callback){
    // Declared variables SEE Scraper function for info
    var request = require('request'),
    cheerio = require('cheerio'),
    urls = [],
    birdNames = [],
    theNum = -1,
    ret = "",
    stringRet = "",
    theString = "";

    // test URL = https://birdsna.org/Species-Account/bna/species/blujay/introduction   just in case

    // This URL is where the website keeps their entire list of birds
    request("https://birdsna.org/Species-Account/bna/species", function(err, resp, body){
        // make sure page is OK
        if(!err && resp.statusCode == 200){
            // load garbage/body of page
            var $ = cheerio.load(body);
            // grab everything in all a.notranslate blocks in scope of div.ListGrid-contents
            // which is the only one in the page so it narrows the search
            $('a.notranslate', 'div.ListGrid-contents').each(function(){
                if($(this).attr('href') != undefined){
                    // pulling url from seciton of body
                    var url = $(this).attr('href');

                }
                // pulling text from seciton of body
                var name = $(this).text();

                // Store them puppies
                // Naming convention is not trash this time
                urls.push(url);
                birdNames.push(name.toLowerCase());


            });
            // run through the list of bird names and look for one that matches
            for(var i = 0; i < birdNames.length; i++) {
                // Looks to see if can find match for the bird. As soon as it does it breaks.
                // If there are multiple type of same bird like Snowy Egret, Brown Egret, etc. it just gets the first one
                if (birdNames[i].indexOf(stringBirdName.toLowerCase()) != -1){
                    theString = birdNames[i]
                    theNum = i
                    break
                }

            }
            // As long as we got something we are gioing to go scrape the page
            // Else we return the bird wasn't found
            if (theString != ""){
                ret = urls[theNum];
                Scraper("https://birdsna.org" + ret, callback)
            } else{
                Returner("The bird wasn't found", callback)
            }

        }

    });


}

// This Function is the start of it all. It will run the code by calling a function which will call another function
// #RippleEffect
function RunStuff(stringBirds, callback){

    var x = GetSpeciesURLS(stringBirds, callback)

}

// All this does is force the input to be gotten since Node.js has funky stuff happen with return
function Returner(getInfo, callback){
  callback(getInfo);
    console.log("Ding Ding: " + getInfo)
}

// Where you input your bird string
RunStuff(["Blue Heron"], s => console.log(s));
