// Used to parse html form data.
const bodyParser = require('body-parser');
// This will parse post data for us.
const urlencodedParser = bodyParser.urlencoded({ extended: false });

// Used to parse html multi form data.
const multer = require('multer');

const uploader = multer();
const cpUpload = uploader.fields([{ name: 'imgUploader', maxCount: 20 }])

// Path related utility methods
const path = require('path');

// Model of the controller.
const database = require(path.dirname(__dirname) + '\\models\\mongoDb');

// File system utility methods
const fs = require('fs');

// Gets the descriptions of the bird classifiers
const descriptor = require(path.dirname(__dirname) + '\\assets\\js\\Descriptor');

// Connection to waton api to
// Recognize images
const watson = require(path.dirname(__dirname) + '\\assets\\js\\watson');
// Used as a queue for promises
const deferred = require('deferred');

// Promise creating function
// Used to convert async methods into promises
const promisify = deferred.promisify;

// Get personal option class to replace null.
const option = require(path.dirname(__dirname) + '\\assets\\js\\Option');

// database promise wrapper for async method.
function mongoFindAllWrapper(condition, callback){
  return database.ImageModel.find(condition, callback);
}

// creation of the promise method
const mongoFindAllPromise = promisify(mongoFindAllWrapper);

// START OF CONTROLLER METHODS FOR VIEWS
// Retrieve app from app.js controller
module.exports = function(app){

  // Used in Temporary storage
  app.use(bodyParser.json());

   const indexList = ['/', '/index'];
   indexList.forEach(function(ele){
     app.get(ele, function(req, res)
     {
       res.render('index');
     });
   });

   app.post("/index", cpUpload, function(req, res) {
            // init image batch
            let mongoBatch = [];
            // Get image files out of
            const files = req.files['imgUploader'];

            if(files.length == 0){
              // No files were uploaded
              console.log('rendering index because no images were uploaded');
              return res.render('index');
            }else{
              // Clear out the collection.
              database.ImageModel.collection.remove({});
              // Go through each file and put the file into json schema
              // Push the object into the image batch
              // TODO: Check if the file is an image.
              files.forEach(function(file){
                // Create js object for the image batch
                var newImage = database.ImageModel({img: { data: file.buffer,
                      contentType: file.mimetype }});
                mongoBatch.push(newImage);
              });
              // Insert all images into mongo
              database.ImageModel.collection.insert(mongoBatch, function(err, docs){
                if(err) throw err;
                console.log('Documents were uploaded to Mongo!');
                // Load new view
                getImages(res);
              });
            }
    });

  app.get('/register', function(req, res){
    res.render('register');
  });

  app.post('/register', urlencodedParser, function(req, res){
    console.log(req.body);
    //TODO: Verify that no other user has the same username
    const newUser = database.UserModel({
      userinfo: {
        username: req.body.username,
        password: req.body.psw,
    }
    });
    database.UserModel.collection.insert(newUser, function(err, user){
      if(err) throw err;
      res.render('index');
    });
  });

  app.get('/login', function(req, res){
    res.render('login');
  });

  app.post('/login', urlencodedParser, function(req, res) {
    console.log(req.body);
    database.UserModel.findOne({userinfo: { username: req.body.username, password: req.body.psw }}, function(err, user) {
      //console.log(user);
      //console.log(err);
    const errorMessage = 'Invalid username or password.';
    if (!user) {
      //console.log(errorMessage);
      res.render('login', { error: errorMessage });
    } else {
      if (req.body.psw === user.userinfo.password) {
        // sets a cookie with the user's info
        req.session.user = user;
        console.log('req session user set: ' + req.session.user);
        res.redirect('index');
      } else {
        console.log('User was found.. '+errorMessage);
        res.render('login', { error: errorMessage });
      }
    }
  });
});

  app.post('/index', function(req, res){
    res.render('index');
  });

  app.get('/contact', function(req, res)
  {
    console.log(req.query);
    res.render('contact', {qs: req.query});
  });

  // Unneeded
  // Routing parameters for dynamic look up
  app.get('/profile/:id', function(req, res){
    // EJS html file
    var data = {age: 29, job:'ninja', hobbies: ['eating', 'fighting', 'fishing']};
    res.render('profile', {id: req.params.id, data: data});
  });
  app.post('/contact', urlencodedParser, function(req, res)
  {
    // the middleware urlencodedParser gives us access to req.body property
    console.log(req.body);
    res.render('contact-success', {data: req.body});
  });

  function getImages(res){
    // finds all items in the collection
      const task = mongoFindAllPromise({});
      task.then(data => {
        // Create empty collection of params objects
        // to feed to watson in order to get the classifier
        let watsonParamsList = [];

        // For each image found from the database.
        // create an entry for watson to id.
        // TODO: should account for size of image
        data.forEach(function(image){
          const p = {
            images_file: image.img.data
          };
          watsonParamsList.push(p);
        });
        // Create a queue of promises to wait on.
        // Once the queue is empty, then we continue
        // to create the view.
        deferred.map(watsonParamsList, function (params) {
          console.log("watsonParams"+params);
          return watson.classify(params);
        })(function (results) {
          console.log("Here");
            viewNames = results.map(result => {
              console.log(result.images[0].classifiers[0].classes[0].class);
              return result.images[0].classifiers[0].classes[0].class;
            });
            console.log('Ready for image view!');
            const descripts = viewNames.map(classifier => descriptor.Get(classifier));
            res.render('imageresult', {
              images: data,
              viewNames: viewNames,
              descripts: descripts
            });
          });
      });
  }
}
