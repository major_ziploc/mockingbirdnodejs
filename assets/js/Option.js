// DEFINITION OF OPTION ----- BEGIN
function Option(value)
{
  // The value that may or may not be there
  this.value = value;
  // The way to get the value out of option.
  // Or if it doesnt exist. An alternate route
  // provided by the client code
  this.Match = function(Some, None)
    {
      return (this.value != null || this.value != undefined) ? Some(value) : None();
    }
}
// The None static object for when a method returns nothing
var None = new Option(null);

// Creation of a Some case.
// If fed null or undefined,
// It will act as None
function Some(value)
{
  return new Option(value);
}
// DEFINITION OF OPTION ----- END

module.exports = {
  Some : Some,
  None: None,
}
