
// Where the fun starts

// Where you input your bird string
//RunStuff("Blue Heron", GetSpeciesURLS)

var birdNamess = ["brown pelican", "northern cardinal", "snowy egret", "blue jay", "blue heron" ]
var birdText = [ "The Brown Pelican is a permanent resident of the coastal marine environment from central North America southward to northern South America. Whether perched atop a piling, panhandling at a fishing pier, or gliding above the surf, this conspicuous and popular seabird is instantly recognizable by its large body, long bill, and enormous gular pouch. Webbing between all 4 toes on each foot makes the Brown Pelican a strong swimmer but an awkward walker. In flight, however, the species comes into its own. Long wings gracefully carry individuals to and from their fishing grounds, and flocks often fly in lines just above the water's surface, slowly rising and falling in a wavelike pattern. Superb fishers, Brown Pelicans are noted for their spectacular head-first dives to trap unsuspecting fish in their expandable pouches. Of the world's 8 pelican species, only the Brown Pelican and the closely related Peruvian Pelican (P. thagus) feed by this plunge-diving method. They also are the only truly marine and predominantly dark-plumaged pelican species.Brown Pelicans are highly social year-round and breed in colonies of up to several thousand pairs. They typically nest on small estuarine or offshore islands, where they are free from disturbance and predation by terrestrial mammals, including humans."
, "The Northern Cardinal (hereafter, cardinal), named for the male's red plumage, is found throughout eastern and central North America from southern Canada into parts of Mexico and Central America. It has taken advantage of moderate temperatures, human habitation, and provisioning at bird feeders to expand its range northward since the early 1800s and has been introduced to California, Hawaii, and Bermuda. A year-round resident, the cardinal is a common visitor to bird feeders in winter, and it has been chosen as state bird in 7 U.S. states.The cardinal is strongly sexually dichromatic—the male brilliant red and the female primarily grayish tan. It is an omnivorous passerine with a diet consisting mainly of seeds, fruits and insects. Plumage color results from ingestion and deposition of carotenoid pigments obtained from the diet during molt and may signal mate quality. New research has shown that brighter males have higher reproductive success and territories with greater vegetation density, and that plumage brightness in both the male (breast color) and female (color of underwing coverts) is positively correlated with parental care (feeding nestlings)."
, "The Snowy's conspicuousness stems from both its white plumage and its active, sometimes frantic foraging behaviors used to capture small fish and crustaceans. This species uses a greater range of foraging behaviors than does any other heron. Behavioral variety does not reflect a broad choice of prey but rather adaptability to a wide range of environmental and social foraging conditions, many of which involve concentrating or increasing availability of prey. Preferred foraging habitats/conditions range from small salt-marsh pools to large freshwater marshes and from solitary to mixed-species aggregations. The Snowy Egret is often the species around which such aggregations form.The breeding behavior of this species is typical of most herons and egrets and is often embellished with the use of graceful nuptial plumes during displays. Pairs typically nest in mixed-species colonies where the Snowy is often one of the most abundant species. Island nest sites are preferred because they are less vulnerable to predators than mainland and peninsular sites."
, "No doubt the Blue Jay was one of the first North American birds to become well known to Europeans. In the sixteenth century, John White made a watercolor illustration of this bird , and Linnaeus  used the text and illustration of the “Blew Jay” by Catesby when he wrote what became the official description of the species. Behavior was ably if colorfully described by Alexander Wilson: the Blue Jay “is distinguished as a kind of beau among feathered tenants of our woods, by the brilliancy of his dress; and like most other coxcombs, makes himself still more conspicuous by his loquacity, and the oddness of his tones and gestures.” Although often disliked because it is sometimes aggressive toward other birds, this small corvid offers excitement and fascination to those who observe it closely. Blue Jays quickly learn to take food provided by humans, and annually many are trapped and banded. As a result, many short reports exist that are based on brief observations in the field (e.g., anting, food items, parasite surveys, predation, migration) or on captives (e.g., longevity, food preferences, growth and development). More in-depth papers describing mast-harvesting and use; self-maintenance; laboratory experiments on optimal foraging, search image, and prey detection; nest success; and analyses of Christmas Bird Count and band return data also exist. Although a few papers on social organization have been published, only rarely have long-term data sets of marked birds in both breeding and nonbreeding seasons been used. Presumably, this is because it is extremely difficult to capture, mark, and observe a high percentage of Blue Jays at one locality. Consequently, our understanding of the breeding biology, demography, and sociality of Blue Jays remains poor."
, "Equally at home in coastal (marine) environments and in fresh water habitats, the Great Blue Heron nests mostly in colonies, commonly large ones of several hundred pairs. Such colonies are often located on islands or in wooded swamps, isolated locations that discourage predation by snakes and mammals and disturbance from humans. Although the species is primarily a fish eater, wading (often belly deep) along the shoreline of oceans, marshes, lakes, and rivers, it also stalks upland areas for rodents and other animals, especially in winter. It has been known to eat most animals that come within striking range. Its well-studied, elaborate courtship displays have correlates on the foraging grounds, where this species can be strongly territorial. The Great Blue Heron weathered the impacts of 20th century North Americans relatively successfully. Although it was hunted heavily for its plumes and some of its wetland habitats were drained or otherwise degraded, many populations have recovered well. Nevertheless, breeding colonies remain vulnerable to disturbance and habitat loss, and climate change and increasing predator populations may bring new challenges."
]
function finalStuff(birdName){
var num = -1
    for(var i = 0; i < birdNamess.length; i++) {
        // Looks to see if can find match for the bird. As soon as it does it breaks.
        // If there are multiple type of same bird like Snowy Egret, Brown Egret, etc. it just gets the first one
        if (birdNamess[i].indexOf(birdName.toLowerCase()) != -1){
            num = i
            break
        }

    }
    if(num != -1){
        return reduceText(birdText[num]);
    } else {
        return "Bird isn't in the database.";
    }

}

function reduceText(text)
{
  if(text.length > 250){
    return text.substring(0,250) + '...';
  }else{
    return text;
  }
}

// Just take this from equaling x to have return something. Using the "finalStuff" function
//var x = finalStuff("egret")
//console.log(x)

module.exports = {
  Get : finalStuff
}
