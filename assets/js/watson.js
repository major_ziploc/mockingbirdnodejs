// Used to create promise methods from async methods
var promisify = require('deferred').promisify;
// Watsons API
var VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');

// Watsons visual_recognition api to id images
var visual_recognition = new VisualRecognitionV3({
  "url": "https://gateway-a.watsonplatform.net/visual-recognition/api",
  "api_key": "b61fd9dcc3feca9d8852fa8d24941a6cedf570c6",
  version_date: VisualRecognitionV3.VERSION_DATE_2016_05_20
});
//{
  //api_key: '0deefec5c4843ed6c7b44f38b1c250a7fab72d77',
  //version_date: VisualRecognitionV3.VERSION_DATE_2016_05_20
//}
function classify_wrapper(option, callback){
  return visual_recognition.classify(option, callback);
}

// Watsons classify async method wrapped into a promise method
var classify = promisify(classify_wrapper);

// TODO: confirm this is unused and place back into the example code.
function processList(paramsList){
  deferred.map(paramsList, function (params) {
    return classify(params);
  })(function (results) {
      results.forEach(result => {
        console.log('Getting result');
        console.log(result.images[0].classifiers[0].classes[0].class);
        //console.log(JSON.stringify(result, null, 2).images[0].classifiers[0].classes[0]);
      });
      console.log('Ready for view!');
  });
}

module.exports = {
  classify : classify,
  processList : processList
}

/* EXAMPLE USE CASE OF RETRIEVING MULTIPLE PIC DECRIPTIONS
 * FROM WATSON IN SYNC
 */
 /*
 var params = {
   images_file: fs.createReadStream('./img/car.jpg')
 };
 var params1 = {
   images_file: fs.createReadStream('./img/car1.jpg')
 };
 var paramsList = [];
 paramsList.push(params);
 paramsList.push(params1);
*/
